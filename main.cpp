#include "mbed.h"
#include "Servo.h"    //Librerias

Servo Thumb(PA_8);   //Servos
Servo IndexF(PA_9);
Servo MiddleF(PA_10);
Servo RinPiF(PC_7);
Servo Elbow1(PA_6); //codo bajar/subir
Servo Wrist(PB_5);
Servo Forearm(PB_4);
Servo Elbow2(PB_3);//codo rotar



//declarar funciones
void Rx_interrupt(void);
void unpack(void);
void work(void);
void control(void); 


//tareas
Thread Work_task(osPriorityNormal);//work task set as priority 2
Thread Control_Task(osPriorityHigh);//comunication task set as priority 1




// objeto serial
Serial pc(SERIAL_TX, SERIAL_RX);
/*---------   Variables serial    ---------------------*/

int rx_buffer[3]={0,0,0};
int next_in =0;
int rx_OK=0;
int rx_01=0;//direccion
int rx_02=0;//servo
int rx_03=0;//Valor



//variables de control

bool Work_time =false;


//variable global 
int degrees_ThumbF=0;
int degrees_IndexF=0;
int degrees_MiddleF=0;
int degrees_RinPi=0;
int degrees_Wrist=0;
int degrees_Forearm=0;
int degrees_Elbow1=0;
int degrees_Elbow2=0;


float c, cc, a, m, d,x;
int y, opcio;

void move_elbow(float x)
{//Pruebas cn sensor: Los grados no son los definitivos.
    if ((-90<=x) and (x<=90)){//grados aproximados 
      Elbow1.position(x);
    }
    
    else{
      printf("Invalid degree. Choose one between 50 and 90.\n");
    }
}

void rotate_elbow(float x)
{
    if ((-90<=x) and (x<=90)){
      Elbow2.position(x);
    }
    
    else{
      printf("Invalid degree. Choose one between 0 and 180.\n");
    }
}

void move_forearm(float x)
{
    if ((-90<=x) and (x<=90)){
      Forearm.position(x);
    }
    
    else{
      printf("Invalid degree. Choose one between 0 and 90.\n");
    }
}

void move_wrist(float x)
{
    if ((0<=x) and (x<=90)){
      Wrist.position(x);
    }
    
    else{
      printf("Invalid degree. Choose one between 0 and 90.\n");
    }
}

void move_Thumb(float x)
{
    if ((-90<=x) and (x<=90)){ //se deberia de mirar al tener todo montado
      Thumb.position(x);
    }
    
    else{
      printf("Invalid degree. Choose one between 0 and 90. \n");
    }
}

void move_IndexF(float x)
{
  if ((-90<=x) and (x<=90)){
    IndexF.position(x); 
  }
  
  else{
    printf("Invalid degree. Choose one between -90 and 90. \n");
  }  
}

void move_MiddleF(float x)
{
  if ((-90<=x) and (x<=90)){
    MiddleF.position(x); 
  }
  
  else{
    printf("Invalid degree. Choose one between -90 and 90. \n");
  }  
}

void move_RinPi(float x)
{
  if ((-90<=x) and (x<=90)){
    RinPiF.position(x); 
  }
  
  else{
    printf("Invalid degree. Choose one between -90 and 90. \n");
  }  
}
int main() {
      //-------- ad uart interrupt---
  pc.attach(&Rx_interrupt, Serial::RxIrq);
  pc.baud(115200); 


  Work_task.start(work);
  Control_Task.start(control);
    
//Thumb.position(0); (FORMA1: con grados)
//Thumb.position(180); 
//Thumb.position(-180);
//Thumb=0.5 (valor entre 0 y 1) (FORMA2: en tanto por 1)  
    Thumb.calibrate(0.001,90);
    IndexF.calibrate(0.001,90);
    MiddleF.calibrate(0.001,90);
    RinPiF.calibrate(0.001,90);
    Elbow1.calibrate(0.001,90);
    Wrist.calibrate(0.001,90);
    Forearm.calibrate(0.001,90);
    Elbow2.calibrate(0.001,90);

}


void control (void)
{
  while(1)
  {
      if (rx_OK==1)// if we read a mesage form uart
    {
      unpack();//--- dechiper the uart message 
      
      
    }
    /*if (Work_time==true){
    Work_task.signal_set(0x1);
    }*/

      Thread::yield();
      Thread::wait(100);

  }

}


void work(void)
{

  while(1)
  {
    //Thread::signal_wait(0x1);//-wait to be executed when needed
    Thumb.position(degrees_ThumbF);
    IndexF.position(degrees_IndexF);
    MiddleF.position(degrees_MiddleF);
    RinPiF.position(degrees_RinPi);
    Wrist.position(degrees_Wrist);
    Forearm.position(degrees_Forearm);
    Elbow1.position(degrees_Elbow1);
    Elbow2.position(degrees_Elbow2);


    //Thread::wait(100);

    //Work_time=false;
    //Work_task.signal_clr(0x1);
  }

}






void Rx_interrupt(void)
{
   rx_buffer[next_in]=pc.getc();
 if (rx_buffer[next_in]==250)
 {
   rx_buffer[0]=250;
   next_in=1;
 }
 else
 {
  if (next_in<=2)
  {
    next_in++;
  }
 }
 if ((next_in==3)&&(rx_buffer[0]==250))
 {
  rx_OK=1;
  next_in=0;
 }

 return;
}


void unpack(void)
{
  rx_01=rx_buffer[1];//id 0X00 per actuators board
  rx_02=rx_buffer[2];//function 0=Actuador  10= control
  
  
//----------POSITIVE DEGREES----------
  if(rx_01==1)
  {
    degrees_ThumbF=rx_02;
    
  }
  
    if(rx_01==2)
  {
    degrees_IndexF=rx_02;
    
  }
  
    if(rx_01==3)
  {
    degrees_MiddleF=rx_02;
    
  }  

    if(rx_01==4)
  {
    degrees_RinPi=rx_02;
    
  }

    if(rx_01==5)
  {
    degrees_Wrist=rx_02;
    
  }

    if(rx_01==6)
  {
    degrees_Forearm=rx_02;
    
  }

    if(rx_01==7)
  {
    degrees_Elbow1=rx_02;
    
  }

    if(rx_01==8)
  {
    degrees_Elbow2=rx_02;
    
  }  
  
//----------NEGATIVE DEGREES----------
 if(rx_01==161)
  {
    degrees_ThumbF=-(rx_02);
    
  }
  
    if(rx_01==162)
  {
    degrees_IndexF=- (rx_02);
    
  }
  
    if(rx_01==163)
  {
    degrees_MiddleF=- (rx_02);
    
  }  

    if(rx_01==164)
  {
    degrees_RinPi=- (rx_02);
    
  }

    if(rx_01==165)
  {
    degrees_Wrist=- (rx_02);
    
  }

    if(rx_01==166)
  {
    degrees_Forearm=- (rx_02);
    
  }

    if(rx_01==167)
  {
    degrees_Elbow1=- (rx_02);
    
  }

    if(rx_01==168)
  {
    degrees_Elbow2=- (rx_02);
    
  }  
    
 pc.putc(48);
 rx_OK=0;
}